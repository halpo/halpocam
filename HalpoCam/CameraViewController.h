//
//  CameraViewController.h
//  HalpoCam
//
//  Created by Paul on 31/03/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>

@property BOOL newMedia;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *message;
@property (weak, nonatomic) IBOutlet UIButton *takeButton;
@property (strong, nonatomic) UIImageView *finga;
- (IBAction)take:(id)sender;
- (IBAction)share:(id)sender;

@end
