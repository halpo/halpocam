//
//  CameraViewController.m
//  HalpoCam
//
//  Created by Paul on 31/03/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "CameraViewController.h"

#if (TARGET_IPHONE_SIMULATOR)
#define kSimulator 1
#else
#define kSimulator 0
#endif

@interface CameraViewController () {
    UIImage *photo;
}

@end

@implementation CameraViewController {
    CGRect viewRect;
    CGRect imageRect;
    float screenWidth;
    float screenHeight;
    BOOL ready;
    int fingerCount;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    screenWidth = [[UIScreen mainScreen] bounds].size.width;
    screenHeight = [[UIScreen mainScreen] bounds].size.height;
    imageRect = CGRectMake(0,kNavHeight,screenWidth,screenHeight-kNavHeight-60);
    _takeButton.frame = CGRectMake(0, screenHeight-60, screenWidth, 60);
    _takeButton.showsTouchWhenHighlighted = YES;
    _imageView.frame = imageRect;
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    NSString *message = @"A Big Fucking Idiot!";
    _message.text = [message uppercaseString];
    _message.hidden = YES;
    self.view.backgroundColor = [UIColor blackColor];
    self.title = @"Say Cheese!";
    fingerCount = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)take:(id)sender {
    UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:@"Image Source" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera", @"Photo Library", nil];
    [as showInView:self.view];
}

#pragma mark UIImagePickerControllerDelegate

-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = info[UIImagePickerControllerOriginalImage];
        _imageView.image = image;
        _message.hidden = NO;
        [self.view bringSubviewToFront:_message];
        [self dismissViewControllerAnimated:YES completion:nil];
        if (_newMedia) {
            UIGraphicsBeginImageContext(imageRect.size);
            CGContextRef resizedContext = UIGraphicsGetCurrentContext();
            CGContextTranslateCTM(resizedContext, 0, -kNavHeight);
            [self.view.layer renderInContext:resizedContext];
            UIImage *flatImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            photo = flatImage;
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"savePhoto"] boolValue]) {
                UIImageWriteToSavedPhotosAlbum(photo,self,@selector(image:finishedSavingWithError:contextInfo:),nil);
            }
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(0, kNavHeight, screenWidth, screenHeight-kNavHeight);
            [btn addTarget:self action:@selector(fingaBlast) forControlEvents:UIControlEventTouchUpInside];
            btn.tag = 1;
            [self.view addSubview:btn];
            ready = YES;
            [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(fingaBlast) userInfo:nil repeats:NO];
        }
    } else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie]) {
        //Handle Movie
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark ActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    BOOL sim = kSimulator;
    if (buttonIndex==0 && !sim) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIImagePickerController *ip = [UIImagePickerController new];
            ip.delegate = self;
            ip.sourceType = UIImagePickerControllerSourceTypeCamera;
            ip.mediaTypes = @[(NSString *) kUTTypeImage];
            ip.allowsEditing = NO;
            ip.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            [self presentViewController:ip animated:YES completion:nil];
            _newMedia = YES;
        }
    } else if (buttonIndex==1) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            UIImagePickerController *ip = [UIImagePickerController new];
            ip.delegate = self;
            ip.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            ip.mediaTypes = @[(NSString *) kUTTypeImage];
            ip.allowsEditing = NO;
            [self presentViewController:ip animated:YES completion:nil];
            _newMedia = YES;
        }
    } else if (buttonIndex==2) {
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Camera" message:@"No Camera on this Device, M8!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark animation

- (void)fingaBlast {
    if (ready) {
        ready = !ready;
        _finga = [[UIImageView alloc] initWithFrame:CGRectMake(0, screenHeight, screenWidth, screenHeight-kNavHeight)];
        viewRect = CGRectOffset(_finga.frame, 0, -screenHeight+kNavHeight);
        _finga.contentMode = UIViewContentModeScaleAspectFit;
        [self.view addSubview:_finga];
        _finga.image = [UIImage imageNamed:@"Finga"];
        [UIView animateWithDuration:1.0 animations:^{
            _finga.frame = viewRect;
        }completion:^(BOOL done) {
            if (done) {
                [UIView animateWithDuration:1.0 animations:^{
                    _finga.frame = CGRectOffset(viewRect, 0, screenHeight-kNavHeight);
                }completion:^(BOOL done) {
                    if (done) {
                        [_finga removeFromSuperview];
                        fingerCount++;
                        ready = !ready;
                        if (fingerCount < 3) {
                            [self fingaBlast];
                        }
                    }
                }];
            }
        }];
    }
}

- (IBAction)share:(id)sender {
    if (photo) {
        NSArray *activityItems;
        NSString *activityText = @"Here's me lookin REAL good in HalpoCam®!!!!";
        UIImage *activityImage = photo;
        activityItems = @[activityText, activityImage];
        
        UIActivityViewController *activityController =
        [[UIActivityViewController alloc]
         initWithActivityItems:activityItems
         applicationActivities:nil];
        
        [self presentViewController:activityController
                           animated:YES completion:nil];
    }
}

@end
