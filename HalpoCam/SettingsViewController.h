//
//  SettingsViewController.h
//  HalpoCam
//
//  Created by Paul on 03/04/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UISwitch *photoSaveSwitch;
- (IBAction)switchChanged:(id)sender;

@end
