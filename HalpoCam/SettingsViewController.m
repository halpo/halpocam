//
//  SettingsViewController.m
//  HalpoCam
//
//  Created by Paul on 03/04/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor blackColor];
    [_photoSaveSwitch setOn:[[[NSUserDefaults standardUserDefaults] valueForKey:@"savePhoto"] boolValue]];
    _versionLabel.text = [NSString stringWithFormat:@"Version %@",[[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleShortVersionString"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)switchChanged:(id)sender {
    [[NSUserDefaults standardUserDefaults] setBool:_photoSaveSwitch.isOn forKey:@"savePhoto"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
