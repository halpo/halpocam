//
//  ViewController.m
//  HalpoCam
//
//  Created by Paul on 31/03/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    int minX,maxX,minY,maxY,spins;
}

@end

@implementation ViewController

- (void)viewDidAppear:(BOOL)animated {
    [self viewDidLoad];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    srand48(time(0));
    _bgView.frame = self.view.frame;
    spins = 1;
    minX = 0;
    maxX = [[UIScreen mainScreen] bounds].size.width-_startButton.frame.size.width;
    minY = kNavHeight;
    maxY =[[UIScreen mainScreen] bounds].size.height-_startButton.frame.size.height;
    _startButton.showsTouchWhenHighlighted = YES;
    [self animateBg];
}

- (void)animateBg {
    CGAffineTransform transform = CGAffineTransformMakeRotation((M_PI*0.5)*spins);
    [UIView animateWithDuration:2.0 delay: 0 options: UIViewAnimationOptionAllowUserInteraction animations:^{
        _bgView.backgroundColor = [self getRandColour];
        [_startButton setFrame:CGRectMake([self randFromRangeMin:minX max:maxX], [self randFromRangeMin:minY max:maxY], _startButton.frame.size.width, _startButton.frame.size.height)];
        _startButton.transform = transform;
        
    }completion:^(BOOL done) {
        if (done) {
            spins++;
            [self animateBg];
        }
    }];
}

- (UIColor *)getRandColour {
    UIColor *color = [UIColor colorWithRed:drand48() green:drand48() blue:drand48() alpha:1.0];
    return color;
}

- (int)randFromRangeMin:(int)min max:(int)max {
    int x = floor(((max-min)*drand48())+min);
    return x;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
